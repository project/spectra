<?php
/**
 * @file
 */

/**
 * Implements hook_install().
 *
 * This will install/reinstall all of the defaults for this module - if you are using custom settings,
 * please CLONE views and groups and use these as a template.
 */
function spectra_install() {
    \Drupal::service('config.installer')->installDefaultConfig('module', 'spectra');
    drupal_set_message(t('Spectra Analytics has been installed.'), 'status', TRUE);
}

/**
 * Implements hook_uninstall().
 */
function spectra_uninstall() {
  \Drupal::service('config.manager')->uninstall('module', 'spectra');
}

/**
 * Add a 'parent_statement' field to the Statement Entity, to support nested fields..
 */
function spectra_update_8101 () {
  $bundle_of = 'spectra_statement';

  $definition_manager = \Drupal::entityDefinitionUpdateManager();


// Create a new field definition.
  $new_status_field = \Drupal\Core\Field\BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Parent Statement'))
    ->setDescription(t('The Parent statement, to which this item is a sub-statement'))
    ->setSetting('target_type', 'spectra_statement')
    ->setSetting('handler', 'default');

  // Install the new definition.
  $definition_manager->installFieldStorageDefinition('parent_statement', $bundle_of, $bundle_of, $new_status_field);
}

/**
 * Convert map field to JSON field for Spectra Data entities
 */
function spectra_update_8102 ()
{
  $types = array(
    'spectra_actor' => 'actor_data',
    'spectra_action' => 'action_data',
    'spectra_context' => 'context_data',
    'spectra_data' => 'data_data',
    'spectra_object' => 'object_data',
  );
  foreach ($types as $key => $value) {
    // Uninstall the old definition
    $schema = \Drupal\Core\Database\Database::getConnection()->schema();
    $schema->dropField($key, $value);
    // Set up new definition
    $bundle_of = $key;
    $definition_manager = \Drupal::entityDefinitionUpdateManager();
    // Create a new field definition.
    $new_field = \Drupal\Core\Field\BaseFieldDefinition::create('jsonb')
      ->setLabel(t('JSON Data'))
      ->setDescription(t('The stored JSON data for this data object.'));
    // Install the new definition.
    $definition_manager->installFieldStorageDefinition($value, $bundle_of, $bundle_of, $new_field);
  }
}

/**
 * Create Spectra Noun and Spectra Verb Objects
 */
function spectra_update_8103 ()
{
  $schema = \Drupal\Core\Database\Database::getConnection()->schema();

  /**
   * Verbs
   */
  $noun_spec = array(
    'description' => 'Spectra Nouns',
    'fields' => array(
      'noun_id' => array(
        'description' => 'The ID of the SpectraNoun entity.',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'auto_increment' => TRUE,
      ),
      'uuid' => array(
        'description' => 'The UUID of the SpectraNoun entity.',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
      ),
      'created' => array(
        'description' => 'The ID of the SpectraNoun entity.',
        'type' => 'int',
        'length' => 11,
      ),
      'changed' => array(
        'description' => 'The ID of the SpectraNoun entity.',
        'type' => 'int',
        'length' => 11,
      ),
      'name' => array(
        'description' => 'The Name of the SpectraNoun entity.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'type' => array(
        'description' => 'The Type of the SpectraNoun entity, which is used for indexing.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'source' => array(
        'description' => 'The Name of the SpectraNoun entity.',
        'type' => 'varchar',
        'length' => 1023,
      ),
      'source_id' => array(
        'description' => 'The Name of the SpectraNoun entity.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'description' => array(
        'description' => 'The Name of the SpectraNoun entity.',
        'type' => 'varchar',
        'length' => 1023,
      ),
    ),
    'primary key' => array('noun_id'),
  );
  $schema->createTable('spectra_noun', $noun_spec);

  $definition_manager = \Drupal::entityDefinitionUpdateManager();
  // Create a new field definition.
  try {
    $new_field = \Drupal\Core\Field\BaseFieldDefinition::create('jsonb')
      ->setLabel(t('JSON Data'))
      ->setDescription(t('Stored JSON data with further information.'));
    // Install the new definition.
    $definition_manager->installFieldStorageDefinition('data', 'spectra_noun', 'spectra_noun', $new_field);
  }
  catch (Exception $e) {
    $new_field = array(
      'type' => 'text',
      'description' => "Stored data with further information.",
      'size' => 'medium',
      'not null' => FALSE
    );
    // Install the new definition.
    $schema->addField('spectra_noun', 'data', $new_field);
  }

  /**
   * Verbs
   */
  $verb_spec = array(
    'description' => 'Spectra Verbs',
    'fields' => array(
      'verb_id' => array(
        'description' => 'The ID of the SpectraNoun entity.',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE,
        'auto_increment' => TRUE,
      ),
      'uuid' => array(
        'description' => 'The UUID of the SpectraNoun entity.',
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
      ),
      'created' => array(
        'description' => 'The ID of the SpectraNoun entity.',
        'type' => 'int',
        'length' => 11,
      ),
      'changed' => array(
        'description' => 'The ID of the SpectraNoun entity.',
        'type' => 'int',
        'length' => 11,
      ),
      'name' => array(
        'description' => 'The Name of the SpectraNoun entity.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'type' => array(
        'description' => 'The Type of the SpectraNoun entity, which is used for indexing.',
        'type' => 'varchar',
        'length' => 255,
      ),
      'description' => array(
        'description' => 'The Name of the SpectraNoun entity.',
        'type' => 'varchar',
        'length' => 1023,
      ),
    ),
    'primary key' => array('verb_id'),
  );
  $schema->createTable('spectra_verb', $verb_spec);

  $definition_manager = \Drupal::entityDefinitionUpdateManager();
  // Create a new field definition.
  try {
    $new_field = \Drupal\Core\Field\BaseFieldDefinition::create('jsonb')
      ->setLabel(t('JSON Data'))
      ->setDescription(t('Stored JSON data with further information.'));
    // Install the new definition.
    $definition_manager->installFieldStorageDefinition('data', 'spectra_verb', 'spectra_verb', $new_field);
  }
  catch (Exception $e) {
    $new_field = array(
      'type' => 'text',
      'description' => "Stored data with further information.",
      'size' => 'medium',
      'not null' => FALSE
    );
    // Install the new definition.
    $schema->addField('spectra_verb', 'data', $new_field);
  }

}

/**
 * Convert Actors, Objects, and Contexts to Nouns, and Actions to Verbs
 */
function spectra_update_8104 ()
{
  $action_query = \Drupal::entityQuery('spectra_action');
  $actions = $action_query->execute();
  $action_props = [
    'uuid' => 'uuid',
    'created' => 'created',
    'changed' => 'changed',
    'action_name' => 'name',
    'action_type' => 'type',
    'action_description' => 'description',
    'action_data' => 'data',
  ];
  foreach ($actions as $action_id) {
    $act = \Drupal\spectra\Entity\SpectraAction::load($action_id);
    $vals = [];
    foreach ($action_props as $key => $val) {
      $vals[$val] = $act->get($key)->getValue();
      $vals[$val] = isset($vals[$val][0]['value']) ? $vals[$val][0]['value'] : $vals[$val];
    }

    $verb = \Drupal\spectra\Entity\SpectraVerb::create($vals);
    $verb->save();
    $act->delete();
  }
  $nouns = [];
  $actor_query = \Drupal::entityQuery('spectra_actor');
  $object_query = \Drupal::entityQuery('spectra_object');
  $context_query = \Drupal::entityQuery('spectra_context');
  $nouns['actor'] = $actor_query->execute();
  $nouns['object'] = $object_query->execute();
  $nouns['context'] = $context_query->execute();

  foreach ($nouns as $bundle => $bundle_nouns) {
    $props = [
      'uuid' => 'uuid',
      'created' => 'created',
      'changed' => 'changed',
      $bundle . '_name' => 'name',
      $bundle . '_type' => 'type',
      $bundle . '_description' => 'description',
      $bundle . '_data' => 'data',
    ];
    if (in_array($bundle, ['actor', 'object'])) {
      $props[$bundle . '_source'] = 'source';
      $props[$bundle . '_source_id'] = 'source_id';
    }
    foreach ($bundle_nouns as $bn) {
      $cb = ucfirst($bundle);
      $class = '\Drupal\spectra\Entity\Spectra' . $cb;
      $bnoun = $class::load($bn);
      $vals = [];
      foreach ($props as $key => $val) {
        $vals[$val] = $bnoun->get($key)->getValue();
        $vals[$val] = isset($vals[$val][0]['value']) ? $vals[$val][0]['value'] : $vals[$val];
      }
      $noun = \Drupal\spectra\Entity\SpectraNoun::create($vals);
      $noun->save();
      $bnoun->delete();
    }
  }
}

/**
 * Remove Actors, Actions, Objects, and Contexts, as these were replaced in update 8104
 */
function spectra_update_8105 () {
  $schema = \Drupal\Core\Database\Database::getConnection()->schema();
  $schema->dropTable('spectra_actor');
  $schema->dropTable('spectra_action');
  $schema->dropTable('spectra_object');
  $schema->dropTable('spectra_context');
}

/**
 * Update the actor_id, action_id, etc. field definitions to the new entity types.
 */
function spectra_update_8106() {
  $definition_manager = \Drupal::entityDefinitionUpdateManager();

  try {
    // Actor
    $definition = $definition_manager->getFieldStorageDefinition('actor_id','spectra_statement');
    $new_definition = \Drupal\Core\Field\BaseFieldDefinition::createFromFieldStorageDefinition($definition)
      ->setSetting('target_type', 'spectra_noun');
    $definition_manager->updateFieldStorageDefinition($new_definition);

    // Action
    $definition = $definition_manager->getFieldStorageDefinition('action_id','spectra_statement');
    $new_definition = \Drupal\Core\Field\BaseFieldDefinition::createFromFieldStorageDefinition($definition)
      ->setSetting('target_type', 'spectra_verb');
    $definition_manager->updateFieldStorageDefinition($new_definition);

    // Object
    $definition = $definition_manager->getFieldStorageDefinition('object_id','spectra_statement');
    $new_definition = \Drupal\Core\Field\BaseFieldDefinition::createFromFieldStorageDefinition($definition)
      ->setSetting('target_type', 'spectra_noun');
    $definition_manager->updateFieldStorageDefinition($new_definition);

    // Context
    $definition = $definition_manager->getFieldStorageDefinition('context_id','spectra_statement');
    $new_definition = \Drupal\Core\Field\BaseFieldDefinition::createFromFieldStorageDefinition($definition)
      ->setSetting('target_type', 'spectra_noun');
    $definition_manager->updateFieldStorageDefinition($new_definition);
  }
  catch (Exception $e) {
    \Drupal::messenger()->addMessage('Definition updates failed.');
  }
}
