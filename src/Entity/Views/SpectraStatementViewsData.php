<?php

namespace Drupal\spectra\Entity\Views;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the spectra_statement entity type.
 */
class SpectraStatementViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}