<?php

namespace Drupal\spectra\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Spectra noun entities.
 *
 * @ingroup spectra
 */
class SpectraNounDeleteForm extends ContentEntityDeleteForm {


}
