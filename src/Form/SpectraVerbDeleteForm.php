<?php

namespace Drupal\spectra\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Spectra verb entities.
 *
 * @ingroup spectra
 */
class SpectraVerbDeleteForm extends ContentEntityDeleteForm {


}
