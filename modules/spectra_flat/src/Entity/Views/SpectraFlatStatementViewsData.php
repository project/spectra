<?php

namespace Drupal\spectra_flat\Entity\Views;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the spectra_statement entity type.
 */
class SpectraFlatStatementViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    return $data;
  }

}